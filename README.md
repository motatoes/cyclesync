How to run
-----------

This is just a cloned mock of a chatbot, developed for the female founders hack.

- clone this repo
- run `npm install`
- run `npm run dev`, this will start a local server where you will be able to see the chatbot.


The bot is very hacky and the interactions in it have been staged.
